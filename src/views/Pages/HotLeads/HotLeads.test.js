import React from 'react';
import ReactDOM from 'react-dom';
import HotLeads from './HotLeads';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HotLeads />, div);
  ReactDOM.unmountComponentAtNode(div);
});
