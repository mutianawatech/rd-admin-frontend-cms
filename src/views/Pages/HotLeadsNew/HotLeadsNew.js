import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row,Form, FormGroup,FormText,Input,Label,InputGroup, } from 'reactstrap';
import { Tbl } from './TblHotLeadsNew';

class HotLeadsNew extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      error: null,
      isLoaded: false,
      items: [],
      list: [],
    };
    this.toggle = this.toggle.bind(this);
  }
    toggle() {
      this.setState({
        modal: !this.state.modal,
      });
  }
  render() {
    const { error, isLoaded, items } = this.state;

    let nomor = 1;
    let lists = [];
    let listss = [];

    items.map(datas => (
      <div>
        {lists = []}
        {lists.push(nomor)}
        {lists.push('<a href="/#/hot_leads_detail/'+datas.customer.mobile_phone_number+'" >'+datas.customer.first_name+'</a>')}
        {lists.push('Ab')}
        {lists.push('<Button onClick={this.toggle} className="mr-1">Show Up</Button>')}
        {listss.push(lists)}
        {nomor++}
      </div>
    ));
    return (
      <div className="animated fadeIn">
        <div className="btn-top">
        <p>Lorem</p>
        <Button className="mr-1 mb-1 btn-right btn-blue text-white"><span>Download</span></Button>
      </div>
      <Tbl data={listss}></Tbl>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Add HotLeads</ModalHeader>
            <ModalBody>
              <form>
              <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="phone_number">Customers Phone Number</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="name" id="date-input" name="phone_number" placeholder="Phone Number" />
                    </Col>
              </FormGroup>
              <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="product_recomendation">Product Recomendation</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="name" id="date-input" name="product_recomendation" placeholder="Product Recomendation" />
                    </Col>
              </FormGroup>
              <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="assign_rd">Assign Retail Dealer</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" name="assign_rd" id="select">
                        <option value="0">Please select</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                      </Input>
                    </Col>
              </FormGroup>
              </form>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Add</Button>{' '}
              <Button color="secondary" onClick={this.toggle}>Cancel</Button>
            </ModalFooter>
          </Modal>
      </div>
      )
  }
  componentDidMount(){
    let listss = [];

    fetch("http://23.101.26.161/api/cms/get-hot-leads-data")
      .then(res => res.json())
      .then(
        (result) => {
           this.setState({
            isLoaded: true,
            items: result
          });
        },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }
}

export default HotLeadsNew;
