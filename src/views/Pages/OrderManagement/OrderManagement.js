import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row,Form, FormGroup,FormText,Input,Label,InputGroup, } from 'reactstrap';
import _ from "lodash";
import ReactTable from "react-table";
import 'react-table/react-table.css'

class OrderManagement extends Component {

  constructor() {
    super();
    this.state = {
      data: [],
      pages: null,
      loading: true
    };
    this.fetchData = this.fetchData.bind(this);
  }
  fetchData(state, instance) {
    // Whenever the table model changes, or the user sorts or changes pages, this method gets called and passed the current table model.
    // You can set the `loading` prop of the table to true to use the built-in one or show you're own loading bar if you want.
    this.setState({ loading: true });
    // Request the data however you want.  Here, we'll use our mocked service we created earlier
    // fetch("http://23.101.25.168/api/cms/get-hot-leads-data")
    let parameter = '?limit='+state.pageSize;

    console.log('State : '+ JSON.stringify(state.filtered));

    parameter = parameter + '&page=' + (parseInt(state.page)+1);


    fetch("http://23.101.25.168/api/cms/order-management-data"+parameter)
    .then(res => res.json())
    .then(res => {
      this.setState({
        data: res,
        pages: res.data.last_page,
        loading: false
      });
    });
  }
  render() {
    const { data, pages, loading } = this.state;

    let nomor = 1;
    let obj_lists = [];
    let arr_lists = [];

    if(!loading){

      data.data.data.map(order_management => (
        <div>
          {obj_lists = []}
          {obj_lists['no'] = nomor}
          {obj_lists['customer'] = <a href={"/#/order_management_detail/"+order_management.customer_number} >{order_management.customer_name}</a>}
          {obj_lists['phone_number'] = order_management.phone_number}
          {obj_lists['product'] = order_management.product_variant_motor.type}
          {obj_lists['payment'] = order_management.payment_detail.name}
          {obj_lists['order_status'] =  order_management.order_status}
          {arr_lists.push(obj_lists)}
          {nomor++}
        </div>
      ));

      // console.log(arr_lists);
    }
    return (
      <div>
        <div className={"row"}>
          <div className={"col"}>
            <input type="file" ref={(ref) => this.upload = ref} style={{ display: 'none' }} onChange={(e) => this.handleSelectedFile(e.target.files[0])}/>
          </div>
        </div>
        <ReactTable
          columns={[
            {
              Header: 'No',
              accessor: 'no'
            },
            {
              Header: 'Customer',
              accessor: 'customer'
            },
            {
              Header: 'Number Phone',
              accessor: 'phone_number'
            },
            {
              Header: 'Name Product',
              accessor: 'product'
            },
            {
              Header: 'Payment',
              accessor: 'payment'
            },
            {
              Header: 'Order Status',
              accessor: 'order_status'
            }
          ]}
          manual // Forces table not to paginate or sort automatically, so we can handle it server-side
          data={arr_lists}
          pages={pages} // Display the total number of pages
          // loading={loading} // Display the loading overlay when we need it
          onFetchData={this.fetchData} // Request new data when things change
          filterable
          defaultPageSize={5}
          className="-striped -highlight"
        />
        <br />
      </div>
      )
  }
}

export default OrderManagement;
