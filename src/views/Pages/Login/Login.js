import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import {Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {PostData} from '../../../services/PostData.js';

class Login extends Component {
  constructor(){
    super();
    this.state = {
      email: '',
      password: '',
      redirectToReferrer: true
    };
    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  login() {
    if(this.state.username != '' && this.state.password != ''){
      Promise.all([PostData('login',this.state)]).then((res) => {
        let responseJson = res;
        console.log(responseJson[0].access_token);
        if(responseJson[0].access_token){
          sessionStorage.setItem('userData',JSON.stringify(responseJson));
          this.setState({redirectToReferrer: false});
          return (<Redirect to={'/home'}/>)
        }
      })
    }else{
      console.log('Error');
    }
  }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
  }

  render() {
    if(sessionStorage.getItem('userData')){
      return (<Redirect to={'/home'}/>)
    }
    return (
        <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" name="email"  placeholder="Email" onChange={this.onChange}  />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" name="password" placeholder="Password" onChange={this.onChange} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                        <input type="button" className="button success" value="Login" onClick={this.login}/>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
