import React from 'react';
import ReactDOM from 'react-dom';
import HotLeadsNew from './HotLeadsNew';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HotLeadsNew />, div);
  ReactDOM.unmountComponentAtNode(div);
});
