export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
      },
    },
    // {
    //   name: 'Product Databases',
    //   url: '/charts',
    //   icon: 'icon-pie-chart',
    // },
    {
      name: 'Hot Leads',
      url: '/hot_leads',
      icon: 'icon-pie-chart',
    },
    {
      name: 'Order Management',
      url: '/order_management',
      icon: 'icon-pie-chart',
    },
    {
      name: 'User Management',
      url: '/hot_leads',
      icon: 'icon-user',
    },
    {
      name: 'Product Database',
      url: '/hot_leads',
      icon: 'fa fa-database',
    },
    {
      name: 'Pricing Database',
      url: '/hot_leads',
      icon: 'fa fa-dollar',
    },
    {
      name: 'Retail Dealer Database',
      url: '/hot_leads',
      icon: 'icon-pie-chart',
    },
    {
      name: 'Customers Database',
      url: '/hot_leads',
      icon: 'icon-user-female',
    },
    {
      name: 'General Settings',
      url: '/hot_leads',
      icon: 'icon-settings',
    },
    // {
    //   name: 'Pages',
    //   url: '/pages',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/login',
    //       icon: 'icon-star',
    //     },
    //     {
    //       name: 'Register',
    //       url: '/register',
    //       icon: 'icon-star',
    //     },
    //     {
    //       name: 'Error 404',
    //       url: '/404',
    //       icon: 'icon-star',
    //     },
    //     {
    //       name: 'Error 500',
    //       url: '/500',
    //       icon: 'icon-star',
    //     },
    //   ],
    // },
  ],
};
