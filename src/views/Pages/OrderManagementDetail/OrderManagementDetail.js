import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Form,
  FormGroup,
  FormText,
  Table,
  Label,
  Input
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

class OrderManagementDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      result: [],
      motor: [],
      list_sales_peoples:[],
      error: null
    }
    this.update_order = this.update_order.bind(this);
  }

  componentDidMount(id_source_RD = null){
    fetch("http://23.101.25.168/api/cms/order-management-data-details",{
      method: "POST",
      mode: "cors", // no-cors, cors, *same-origin
      headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          // "Content-Type": "application/json",
      },
      body: "customer_number="+this.props.match.params.phone_number,
    })
    .then(res => res.json())
    .then(
      (result) => {
         this.setState({
          isLoaded: true,
          result
        });
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
    axios({
      get:'get',
      url:'http://23.101.25.168/api/cms/data-for-update-order-management'
    })
    .then((response) => {
      this.setState({
        motor: response.data.data ? response.data.data.product_variant: []
      })
      console.log(response);
    }).catch((error) => {
      this.setState({
        error: error
      })
    });
    axios({
      method:'post',
      url:'http://23.101.25.168/api/cms/get-data-sp',
      headers: {"Content-Type": "application/x-www-form-urlencoded"},
      data: 'idRD='+id_source_RD
    })
    .then((response) => {
      this.setState({
        list_sales_peoples: response.data ? response.data.data : []
      })
    }).catch((error) => {
      this.setState({
        error: error
      })
    });
  }
  update_order(){
    // console.log('Customer ID : '+this.state.customer_id);
    // console.log('ID Salespeople : '+this.state.idSP);
    axios({
      method:'post',
      url:'http://23.101.25.168/api/cms/update-order-management-data',
      headers: {"Content-Type": "application/x-www-form-urlencoded"},
      data: 'mokita_transact_id='+this.state.mokita_transact_id+'&idSP='+this.state.idSP
    })
    .then((response) => {
      this.setState({
        // list_sales_peoples: response.data ? response.data.data : [],
        // modal_assign_salespeople: !this.state.modal_assign_salespeople
      })
      this.toast_assign_salespeople = toast('Assign Salespeople Success', {
        type: toast.TYPE.SUCCESS,
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    },
    (error) => {
      this.setState({
        error: error,
        modal_assign_salespeople: !this.state.modal_assign_salespeople
      })
      this.toast_assign_salespeople = toast('Assign Salespeople Failed', {
        type: toast.TYPE.ERROR,
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    })
  }

  render() {
    const {isLoaded, result, error ,motor, list_sales_peoples} = this.state;

    if(error){
      return (
        <div>
          Error
        </div>
      )
    }else if (!isLoaded) {
      return (
        <div>
          Loading...
        </div>
      )
    }else{
      return (
        <div className="animated fadeIn">
           
            <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Customer Detail
                </CardHeader>
                <CardBody>
                  <table class="table table-striped table-hover">
                  <tbody>
                    <tr><td>ID:</td>
                      <td>
                        <strong>
                            <FormGroup>
                              <Input type="text" id="name" value={result.data.mokita_transact_id} disabled />
                            </FormGroup>
                        </strong>
                      </td>
                    </tr>
                    <tr><td>Nama:</td>
                      <td>
                        <strong>
                          <FormGroup>
                              <Input type="text" id="name" defaultValue={result.data.customer_name} required />
                          </FormGroup>
                        </strong>
                      </td>
                    </tr>
                    <tr><td>No. KTP:</td>
                      <td>
                        <strong>
                            <FormGroup>
                              <Input type="text" id="name" defaultValue={result.data.id_card_num} required />
                            </FormGroup>
                        </strong>
                      </td>
                    </tr>
                    <tr><td>Alamat:</td>
                      <td>
                        <strong>
                            <FormGroup>
                              <Input type="text" id="name"  defaultValue={result.data.address} required />
                            </FormGroup>
                        </strong>
                      </td>
                    </tr>
                    <tr><td>No Telephone:</td>
                        <td>
                          <strong>
                            <FormGroup>
                              <Input type="text" id="name"  defaultValue={result.data.customer_number} required />
                            </FormGroup>
                          </strong>
                        </td>
                    </tr>
                  </tbody>
                  </table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xl={6}>
              <Card>
                <CardHeader>
                  <Row>
                    <Col xl={6}>Assigned Sales People</Col>
                  </Row>
                </CardHeader>
                <CardBody>
                <table class="table table-striped table-hover">
                  <tbody>
                    <tr><td><strong>
                    <Input type="select" name="assign_rd" id="select">
                    <option value="">Please select</option>
                    {console.log('list_sales_peoples : ')}
                    {console.log(list_sales_peoples)}
                    {list_sales_peoples ? list_sales_peoples.map(list_sales_people => (
                      <option value={list_sales_people.id}>{list_sales_people.username}</option>
                    )) : ''}
                  </Input>
                  
                  <Input type="select">
                    <option value={result.data.product_variant_motor.id}>{result.data.product_variant_motor.type}</option>
                    {console.log('List Customer : ')}
                    {console.log(motor)}
                    {motor ? motor.map(product_variant => (
                      <option value={product_variant.id}>{product_variant.type}</option>
                    )) : []}
                    </Input>
                    </strong></td></tr>
                  </tbody>
                  </table>
                </CardBody>
              </Card>
              <Card>
                <CardHeader>
                  <Row>
                    <Col xl={6}>Order Status</Col>
                  </Row>
                </CardHeader>
                <CardBody>
                <table class="table table-striped table-hover">
                  <tbody>
                  <tr><td>Order Status:</td>
                  <td><strong>{result.data.order_status}</strong></td></tr>
                  </tbody>
                  </table>
                </CardBody>
              </Card>
            </Col>

            <Col xl={6}>
              <Card>
                <CardHeader>
                  <Row>
                    <Col xl={6}>Motorbikes Detail</Col>
                  </Row>
                </CardHeader>
                <CardBody>
                <table class="table table-striped table-hover">
                  <tbody>
                    <tr><td>Kategori:</td><td><strong>{result.data.product_variant_motor.product_category}</strong></td></tr>
                    <tr><td>Unit:</td><td><strong>{result.data.product_variant_motor.product_unit}</strong></td></tr>
                    <tr><td>Md SKU:</td><td><strong>{result.data.product_variant_motor.md_sku_id}</strong></td></tr>
                    <tr><td>Nama Motor:</td><td><strong>
                    <Input type="select">
                    <option value={result.data.product_variant_motor.id}>{result.data.product_variant_motor.type}</option>
                    {console.log('List Customer : ')}
                    {console.log(motor)}
                    {motor ? motor.map(product_variant => (
                      <option value={product_variant.id}>{product_variant.type}</option>
                    )) : []}
                    </Input>
                    </strong></td></tr>
                  </tbody>
                  </table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <Row>
                    <Col xl={6}>Payment Details</Col>
                  </Row>
                </CardHeader>
                <CardBody>
                <table class="table table-striped table-hover">
                  <tbody>
                  <tr><td>Metode Pembayaran:</td><td><strong>{result.data.payment_detail.name}</strong></td></tr>
                  <tr><td>Metode Transaksi:</td><td><strong>{result.data.payment_detail.description}</strong></td></tr>
                  </tbody>
                  </table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xl={12}>
              <Button type="submit" size="xl" color="primary" className="btn-submit" onClick={this.update_order}><i className="fa fa-dot-circle-o"></i> Submit</Button>
            </Col>
          </Row>
        </div>
      )
    }

  }
}


export default OrderManagementDetail;
