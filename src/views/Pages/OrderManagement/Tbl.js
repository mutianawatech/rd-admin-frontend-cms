import './css/jquery.dataTables.css';
import React, { Component } from 'react';

const $ = require('jquery');
$.DataTable = require('datatables.net')

export class Tbl extends Component{
    componentWillReceiveProps(nextProps){
        this.$el = $(this.el)
        this.$el.DataTable(
            {
            data: nextProps.data,
            destroy: true,
            columns: [
                { title: "No" },
                { title: "Customer" },
                { title: "Assign Sales RD" },
                { title: "Product"},
                { title: "Status Order"},
                ]
            }
        )
    }
    render(){
        return<div>
            <table className="display" width="100%" ref={el => this.el = el}></table>
            </div>
    }
}
