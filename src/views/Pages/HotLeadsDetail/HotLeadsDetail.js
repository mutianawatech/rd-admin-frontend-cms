import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

class HotLeadsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      result: [],
      error: null
    }
  }

  componentDidMount(){
    fetch("http://23.101.25.168/api/cms/hot-leads-data-details",{
      method: "POST",
      mode: "cors", // no-cors, cors, *same-origin
      headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          // "Content-Type": "application/json",
      },
      body: "phone_number="+this.props.match.params.phone_number,
    })
    .then(res => res.json())
    .then(
      (result) => {
         this.setState({
          isLoaded: true,
          result
        });
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }

  render() {
    const {isLoaded, result, error} = this.state;

    if(error){
      return (
        <div>
          Error
        </div>
      )
    }else if (!isLoaded) {
      return (
        <div>
          Loading...
        </div>
      )
    }else{
      return (
        <div className="animated fadeIn">
          <Row>
          <Col xl={6}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Hot Leads Detail
              </CardHeader>
              <CardBody>
                <table class="table table-striped table-hover">
                <tbody>
                  <tr><td>id:</td><td><strong>{this.state.result.id}</strong></td></tr>
                  <tr><td>Nama:</td><td><strong>{this.state.result.first_name}</strong></td></tr>
                  <tr><td>No. KTP:</td><td><strong>{this.state.result.ktp_id_number}</strong></td></tr>
                  <tr><td>Email:</td><td><strong>{this.state.result.email}</strong></td></tr>
                  <tr><td>Alamat:</td><td><strong>{this.state.result.ktp_address}</strong></td></tr>
                </tbody>
                </table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <Row>
                  <Col xl={6}>Product Recomendation</Col>
                  <Col xl={6}>Previous Motorbike Type</Col>
                </Row>
              </CardHeader>
              <CardBody>
              <Row>
                  <Col xl={6}>
                  <p>1. Honda Blade</p>
                  </Col>
                  <Col xl={6}>
                  1. Honda Vario
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xl={6}>
            <Card>
              <CardHeader>
                <Row>
                  <Col xl={6}>Assigned RD</Col>
                </Row>
              </CardHeader>
              <CardBody>
              </CardBody>
            </Card>
          </Col>

          <Col xl={6}>
            <Card>
              <CardHeader>
                <Row>
                  <Col xl={6}>Assigned Sales People</Col>
                </Row>
              </CardHeader>
              <CardBody>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
               Lead Status
              </CardHeader>
              <CardBody>
                
              </CardBody>
            </Card>
          </Col>
        </Row>
        </div>
      )
    }

  }
}

export default HotLeadsDetail;
